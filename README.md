# Project_Challenge

## Description

Familiarize yourself with parallel partial reduction 
[trees](https://www.sciencedirect.com/topics/computer-science/partial-reduction).

In this exercise, your task is to scan left on a given input array 
and output a new array consisting of the maximum values 
to the left of a given index in the original array.

Example:
```
In: [0, 0, 1, 5, 2, 3, 6]

Out: [0, 0, 1, 5, 5, 5, 6]
```

### Task 1
Using the partial parallel reduction tree paradigm, implement the methods described in [ScanInterface](./src/main/scala/com/intellias/challenge/ScanInterface.scala), 
making use of the parallelism abstractions provided. You should not utilize
data or function parallel constructs that have not been provided.

Utilize good judgment when choosing side-effecting vs pure implementations to
blend good functional style with performance.

Note that a [trivial sequential
implementation](./src/main/scala/com/intellias/challenge/SequentialScan.scala) has been provided for reference.

### Task 2
Using scalameter, compare performance between a fully sequential implementation
and your parallel implementation, and provide some recommendations for optimal
threshold values for your system.

#### Answer 

The test was performed on input arrays size 300k-2.1kk, with step 300k
Looking at my results apparently threshold ~ 30 is the amount where gain from extending it becomes significantly small.

However, results form sequential implementation are way better than from parallel implementation.
I suspect the overhead of building a tree, which significantly influence execution time for small dataset

### Task 3
Using your observations from Task 2, extrapolate to general systems.

#### Answer
Seems like the trivial sequential implementation is quicker than the parallel one, but the sequential one 
don't scale horizontally, so simply can't be used for big datasets. For a big dataset
parallel implementation based on divide and conquer solution, even if it has worse performance on 
small dataset, is the one working for the big dataset in a distributed system.
