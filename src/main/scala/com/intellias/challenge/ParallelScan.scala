package com.intellias.challenge

import scala.util.chaining.*

object ParallelScan extends ScanInterface {
  override def sequentialUpsweep(input: Array[Int], from: Int, until: Int): Int =
  // input.slice(from, until).max
    (from + 1).until(until).foldLeft(input(from)) {
      case (currentMax, idx) => currentMax max input(idx)
    }

  override def upsweep(input: Array[Int], from: Int, until: Int, threshold: Int): Tree =
    // input.slice(from, until).grouped(threshold)
    val distance = until - from
    if (distance > threshold) {
      val pivot = from + distance / 2
      (Tree.Node.apply _).tupled(common.parallel(upsweep(input, from, pivot, threshold), upsweep(input, pivot, until, threshold)))
    } else {
      Tree.Leaf(from, until, sequentialUpsweep(input, from, until))
    }

  override def sequentialDownsweep(input: Array[Int], output: Array[Int], startingValue: Int, from: Int, until: Int): Unit =
    from.until(until).foldLeft(startingValue) {
      case (currentMax, idx) =>
        math.max(currentMax, input(idx)).tap(output(idx) = _)
    }

  override def downsweep(input: Array[Int], output: Array[Int], startingValue: Int, tree: Tree): Unit =
    tree match
      case Tree.Node(left, right) =>
        common.parallel(downsweep(input, output, startingValue, left), downsweep(input, output, left.maxPrevious max startingValue, right))
      case e: Tree.Leaf =>
        sequentialDownsweep(input, output, startingValue, e.from, e.until)

  override def scan(input: Array[Int], output: Array[Int], threshold: Int): Unit =
    val tree = upsweep(input, 0, input.length, threshold)
    downsweep(input, output, input(0), tree)
}
