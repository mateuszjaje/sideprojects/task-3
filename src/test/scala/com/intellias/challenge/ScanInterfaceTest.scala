package com.intellias.challenge

import org.scalameter.api._
import scala.util.Random

object ScanInterfaceTest extends Bench.ForkedTime {
  val inputs: Gen[Array[Int]] = for {
    size <- Gen.range("size")(300000, 2100000, 300000)
  } yield Array.fill(size)(Random.nextInt)

  val tresholds = Gen.range("threshold")(2, 50, 10)

  val combined = inputs.cross(tresholds)

  override val reporter = Reporter.Composite(
    HtmlReporter[Double](), LoggingReporter[Double]()
  )

  measure method "sequential scan" in {
    using(inputs) curve "input" in { inputData =>
      SequentialScan.scan(inputData, new Array[Int](inputData.length))
    }
  }
  measure method "parallel scan" in {
    using(combined) curve "input" in { case (inputData, threshold) =>
      ParallelScan.scan(inputData, new Array[Int](inputData.length), threshold)
    }
  }


}
